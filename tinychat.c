/*
 * tinychat.c - [Starting code for] a web-based chat server.
 *
 * Based on:
 *  tiny.c - A simple, iterative HTTP/1.0 Web server that uses the 
 *      GET method to serve static and dynamic content.
 *   Tiny Web server
 *   Dave O'Hallaron
 *   Carnegie Mellon University
 */
#include "csapp.h"
#include "dictionary.h"
#include "more_string.h"

void doit(int fd);
dictionary_t *read_requesthdrs(rio_t *rp);
void read_postquery(rio_t *rp, dictionary_t *headers, dictionary_t *d);
void parse_query(const char *uri, dictionary_t *d);
void serve_form(int fd, const char *pre_content, dictionary_t * query);
void clienterror(int fd, char *cause, char *errnum, 
		 char *shortmsg, char *longmsg);
static void print_stringdictionary(dictionary_t *d);
void handle_convo(int fd,dictionary_t *query);
void handle_say(int fd, dictionary_t * query);
static dictionary_t * topics;
static sem_t lock;
#if 0
static char * tostringdictionary(dictionary_t *d)
{
  int i, count;
  char * this = "";
  count = dictionary_count(d);
  for (i = 0; i < count; i++) {
    this = append_strings(this,dictionary_key(d, i), "=", dictionary_value(d, i),NULL);
  }
  return this;
}
#endif
int main(int argc, char **argv) 
{
  Sem_init(&lock,0,1);
  int listenfd, connfd;
  char hostname[MAXLINE], port[MAXLINE];
  socklen_t clientlen;
  struct sockaddr_storage clientaddr;
  topics = make_dictionary(COMPARE_CASE_SENS, free);
  /* Check command line args */
  if (argc != 2) {
    fprintf(stderr, "usage: %s <port>\n", argv[0]);
    exit(1);
  }

  listenfd = Open_listenfd(argv[1]);

  /* Don't kill the server if there's an error, because
     we want to survive errors due to a client. But we
     do want to report errors. */
  exit_on_error(0);

  /* Also, don't stop on broken connections: */
  Signal(SIGPIPE, SIG_IGN);

  while (1) {
    clientlen = sizeof(clientaddr);
    connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);
    if (connfd >= 0) {
      pthread_t th;
      Getnameinfo((SA *) &clientaddr, clientlen, hostname, MAXLINE, 
                  port, MAXLINE, 0);
      printf("Accepted connection from (%s, %s)\n", hostname, port);
      Pthread_create(&th,NULL,doit,&connfd);
      doit(connfd);
      Close(connfd);
    }
  }
}

/*
 * doit - handle one HTTP request/response transaction
 */
void doit(int fd) 
{
  char buf[MAXLINE], *method, *uri, *version;
  rio_t rio;
  dictionary_t *headers, *query;

  /* Read request line and headers */
  Rio_readinitb(&rio, fd);
  if (Rio_readlineb(&rio, buf, MAXLINE) <= 0)
    return;
  printf("%s", buf);
  
  if (!parse_request_line(buf, &method, &uri, &version)) {
    clienterror(fd, method, "400", "Bad Request",
                "TinyChat did not recognize the request");
  } else {
    if (strcasecmp(version, "HTTP/1.0")
        && strcasecmp(version, "HTTP/1.1")) {
      clienterror(fd, version, "501", "Not Implemented",
                  "TinyChat does not implement that version");
    } else if (strcasecmp(method, "GET")
               && strcasecmp(method, "POST")) {
      clienterror(fd, method, "501", "Not Implemented",
                  "TinyChat does not implement that method");
    } else {
      headers = read_requesthdrs(&rio);

      /* Parse all query arguments into a dictionary */
      query = make_dictionary(COMPARE_CASE_SENS, free);
      parse_uriquery(uri, query);
      if (!strcasecmp(method, "POST"))
	read_postquery(&rio, headers, query);	
      if (!strcasecmp(method, "GET"))
	parse_query(uri, query);	

      //handle various browser posts
      
      //service query support
      if(starts_with("/conversation",uri))
	{
	  handle_convo(fd,query); 
	}
      else if(starts_with("/say",uri))
	{
	  handle_say(fd,query);
	}
      else if(starts_with("/import",uri))
	{
	  
	}
      else
	{
	  if(dictionary_get(query,"Topic")==NULL&&
	     dictionary_get(query,"nombre")==NULL)
	    serve_form(fd, "Welcome to TinyChat",query);
	  else 
	    {
	      char * cont = strdup("");
	      cont = dictionary_get(query,"content");
	      if(cont!=NULL&&strlen(cont)>0)
		{
		  P(&lock);
		    //set_me is convo+message
		  char *check = dictionary_get(topics,dictionary_get(query,"Topic"));
		  if(check == NULL)
		    check = "";
		  char * set_me = append_strings(check,
						 "\r\n",
						 dictionary_get(query,"nombre"),": ",
						 dictionary_get(query,"content"),"\r\n",
						 NULL);
		  
		  dictionary_set(topics,dictionary_get(query,"Topic"),set_me);
		  V(&lock);
		}
	      //}
	      serve_form(fd, "Topic",query);
	    }
	}
      
      /* Clean up */
      free_dictionary(query);
      free_dictionary(headers);
    }
    
    /* Clean up status line */
    free(method);
    free(uri);
    free(version);
  }
}
/*
 * read_requesthdrs - read HTTP request headers
 */
dictionary_t *read_requesthdrs(rio_t *rp) 
{
  char buf[MAXLINE];
  dictionary_t *d = make_dictionary(COMPARE_CASE_INSENS, free);

  Rio_readlineb(rp, buf, MAXLINE);
  printf("%s", buf);
  while(strcmp(buf, "\r\n")) {
    Rio_readlineb(rp, buf, MAXLINE);
    printf("%s", buf);
    parse_header_line(buf, d);
  }
  
  return d;
}

void read_postquery(rio_t *rp, dictionary_t *headers, dictionary_t *dest)
{
  char *len_str, *type, *buffer;
  int len;
  
  len_str = dictionary_get(headers, "Content-Length");
  len = (len_str ? atoi(len_str) : 0);

  type = dictionary_get(headers, "Content-Type");
  
  buffer = malloc(len+1);
  Rio_readnb(rp, buffer, len);
  buffer[len] = 0;

  if (!strcasecmp(type, "application/x-www-form-urlencoded")) {
    parse_query(buffer, dest);
  }
  //printf(buffer);
  //printf("\n\n\n\n");
  free(buffer);
}

static char *ok_header(size_t len, const char *content_type) {
  char *len_str, *header;
  
  header = append_strings("HTTP/1.0 200 OK\r\n",
                          "Server: TinyChat Web Server\r\n",
                          "Connection: close\r\n",
                          "Content-length: ", len_str = to_string(len), "\r\n",
                          "Content-type: ", content_type, "\r\n\r\n",
                          NULL);
  free(len_str);

  return header;
}
/*
 *
 */
void handle_convo(int fd,dictionary_t *query)
{

  size_t len;
  char *body, *header;
  P(&lock);
  body = dictionary_get(topics,dictionary_get(query,"Topic"));
  if(body==NULL)
    body = "gurble";
  V(&lock);
  len = strlen(body);
  
  /* Send response headers to client */
  
  header = ok_header(len, "text/plaintext; charset=utf-8");
  Rio_writen(fd, header, strlen(header));
  printf("Response headers:\n");
  printf("%s", header);
  
  free(header);
  
  /* Send response body to client */
  Rio_writen(fd, body, len);
}
void handle_say(int fd, dictionary_t * query)
{

  size_t len;
  char *body, *header;
  
  if(strlen(dictionary_get(query,"content")))
    {
      P(&lock);
      body = append_strings(dictionary_get(topics,dictionary_get(query,"topic")),
			    "\r\n",
			    dictionary_get(query,"name"),": ",
			    dictionary_get(query,"content"),"\r\n",
			    NULL);
      
      dictionary_set(topics,dictionary_get(query,"topic"),body);
      V(&lock);
    }
  len = strlen(body);
  
  /* Send response headers to client */
  
  header = ok_header(len, "text/plaintext; charset=utf-8");
  Rio_writen(fd, header, strlen(header));
  printf("Response headers:\n");
  printf("%s", header);
  
  free(header);
  
  /* Send response body to client */
  Rio_writen(fd, body, len);
}
/*
 * serve_form - sends a form to a client
 */
void serve_form(int fd, const char *pre_content, dictionary_t *query)
{
  size_t len;
  char *body, *header;
  if(!strcmp(pre_content,"Welcome to TinyChat"))
    {
  body = append_strings("<html><body>\r\n",
                        "<p>Welcome to TinyChat</p>",
                        "\r\n<form action=\"reply\" method=\"post\"",
                        " enctype=\"application/x-www-form-urlencoded\"",
                        " accept-charset=\"UTF-8\">\r\n",
			" NAME:<br>",
                        "<input type=\"text\" name=\"nombre\"><br>\r\n",
			" TOPIC:<br>",
                        "<input type=\"text\" name=\"Topic\">\r\n",
                        "<input type=\"submit\" value=\"Join\">\r\n",
                        "</form>\r\n",
			"</body></html>\r\n",
                        NULL);
  
    }
  else if(!strcmp(pre_content,"Topic"))
    {
      char * convo;
      convo = dictionary_get(topics,dictionary_get(query,"Topic"));
      if(convo==NULL)
	convo = "";
      body = append_strings("<html><body>\r\n",
			    "<p>Topic = ",
			    dictionary_get(query,"Topic"),"\0",
			    "</p>",
			    "\r\n<form action=\"reply\" method=\"post\"",
			    " enctype=\"application/x-www-form-urlencoded\"",
			    " accept-charset=\"UTF-8\">\r\n",
			    "<p>",
			"<textarea rows=\"10\" cols=\"30\">",
			    convo,"\0",
			    "</textarea>\r\n",
			    "<p>",
			    dictionary_get(query,"nombre"),":",
			    "<input type=\"text\" name=\"content\">\r\n",
			    "<input type=\"submit\" value=\"Send\">\r\n",
			    "<input type=\"hidden\" name = \"nombre\" value=\"",
			dictionary_get(query,"nombre"),"\">",
			    "<input type=\"hidden\" name = \"Topic\" value=\"",
			    dictionary_get(query,"Topic"),"\">",
			    "</form>\r\n",
			    "</body></html>\r\n",
			    NULL);
    }
  else if (!strcmp(pre_content,"say"))
    {	      
    }
  len = strlen(body);

  /* Send response headers to client */

  header = ok_header(len, "text/html; charset=utf-8");
  Rio_writen(fd, header, strlen(header));
  printf("Response headers:\n");
  printf("%s", header);

  free(header);

  /* Send response body to client */
  Rio_writen(fd, body, len);

  free(body);
}

/*
 * clienterror - returns an error message to the client
 */
void clienterror(int fd, char *cause, char *errnum, 
		 char *shortmsg, char *longmsg) 
{
  size_t len;
  char *header, *body, *len_str;

  body = append_strings("<html><title>Tiny Error</title>",
                        "<body bgcolor=""ffffff"">\r\n",
                        errnum, " ", shortmsg,
                        "<p>", longmsg, ": ", cause,
                        "<hr><em>The Tiny Web server</em>\r\n",
                        NULL);
  len = strlen(body);

  /* Print the HTTP response */
  header = append_strings("HTTP/1.0 ", errnum, " ", shortmsg,
                          "Content-type: text/html; charset=utf-8\r\n",
                          "Content-length: ", len_str = to_string(len), "\r\n\r\n",
                          NULL);
  free(len_str);
  
  Rio_writen(fd, header, strlen(header));
  Rio_writen(fd, body, len);

  free(header);
  free(body);
}

static void print_stringdictionary(dictionary_t *d)
{
  int i, count;

  count = dictionary_count(d);
  for (i = 0; i < count; i++)
    {
      printf("%s=%s\n",
	     dictionary_key(d, i),
	     (const char *)dictionary_value(d, i));
    }
  printf("\n");
}
