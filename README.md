Tiny Chat Server:
An http chat server that can run multiple conversations with concurrent clients. I implemented the doit, serve_form, and main functions.
The rest of the application framework and rackett tests are attributed to Prof. Matthew Flatt of the University of Utah (http://www.cs.utah.edu/~mflatt/).